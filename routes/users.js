// ! userInfo: name, email, pendingTasks, dateCreated(auto)

var User = require('../models/user');
var Task = require('../models/task');

module.exports = (router) => {
  var userRoute = router.route('/users');
  var ret = {
    "message": "OK",
    "data": {}
  }

  // GET: Respond with a List of users
  userRoute.get((req, res) => {
    var filter = {};
    if ("where" in req.query) {
      var filter = JSON.parse(req.query.where);
    }
    var sort = {};
    if ("sort" in req.query) {
      var sort = JSON.parse(req.query.sort);
    }
    var select = {};
    if ("select" in req.query) {
      var select = JSON.parse(req.query.select);
    }
    var skip = 0;
    if ("skip" in req.query) {
      var skip = JSON.parse(req.query.skip);
    }
    var limit = 0;
    if ("limit" in req.query) {
      var limit = JSON.parse(req.query.limit);
    }
    var count = false;
    if ("false" in req.query) {
      var count = JSON.parse(req.query.count);
    }
    // after filtered
    var promise = new Promise((resolve, reject) => {
      User.find(filter, (err, users) => {
        if (err) {
          reject(err);
        } else {
          resolve(users);
        }
      })
        .sort(sort).select(select).skip(skip).limit(limit);
    });
    promise.then((users) => {
      ret.data = users;
      //! if set to true, return the count of documents that match the query (instead of the documents themselves)
      if (count) {
        ret.data = users.length;
      }
      ret.message = "OK";
      res.json(200, ret);
      console.log(req);
      return router;
    })
      .catch(err => {
        console.log(err);
        ret.message = "Error";
        ret.data = "Server Error";
        res.json(500, ret);
        return router;
      });
  });

  // POST: Create a new user. Respond with details of new user
  userRoute.post((req, res) => {
    const userInfo = {
      "name": req.param("name"),
      "email": req.param("email"),
      "pendingTasks": req.param("pendingTasks"),
    };
    //! Users cannot be created (or updated) without a name or email
    const invalidUserInfo = (attribute) => {
      return typeof (attribute) === "undefined";
    }
    if (invalidUserInfo(userInfo.name)) {
      ret.message = "Error";
      ret.data = "Users cannot be created (or updated) without a name";
      res.status(404).json(ret);
      return router;
    }
    if (invalidUserInfo(userInfo.email)) {
      ret.message = "Error";
      ret.data = "Users cannot be created (or updated) without a email";
      res.status(404).json(ret);
      return router;
    }
    if (invalidUserInfo(userInfo.pendingTasks)) {
      userInfo.pendingTasks = [];
    } else {
      userInfo.pendingTasks = JSON.parse(userInfo.pendingTasks);
    }
    //! Multiple users with the same email cannot exist
    User.findOne({ email: userInfo["email"] })
      .then((user) => {
        if (user) {
          res.status(400).send({ "message": "Error", "data": "Multiple users with the same email cannot exist" });
          return router;
        } else {
          var newTasks = userInfo.pendingTasks;
          Promise.all(newTasks.map((taskId) => {
            var task = Task.findById(taskId)
              .catch((err) => {
                console.log(err);
              });
            return task;
          }))
            .then((tasks) => {
              var taskList = [];
              tasks.forEach((task) => {
                if (task) {
                  console.log(task._id);
                  taskList.push(task._id);
                  User.findByIdAndUpdate(task.assignedUser, { $pull: { "pendingTasks": task._id } })
                    .catch(err => {
                      console.log(err);
                    });
                }
              });
              return taskList;
            })
            .then(taskList => {
              userInfo.pendingTasks = taskList;
              // create new user
              var newUser = new User(userInfo);
              newUser.save().then(newUser => {
                Task.updateMany({ "_id": { $in: newUser.pendingTasks } },
                  { $set: { "assignedUser": newUser._id, "assignedUserName": newUser.name } })
                  .catch(err => {
                    console.log(err);
                  });
                return newUser;
              })
                .then(newUser => {
                  res.status(201).send({ message: "OK", "data": newUser });
                  return router;
                });
            });
        }
      });
  });

  return router;
}