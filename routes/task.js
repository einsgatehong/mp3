let User = require('../models/user');
let Task = require('../models/task');
const task = require('../models/task');

module.exports = (router) => {
  let taskRoute = router.route('/tasks/:id');
  let ret = {
    "message": "OK",
    "data": {}
  }

  // GET: Respond with details of specified task or 404 error
  taskRoute.get((req, res) => {
    var id = req.params.id;
    var promise = new Promise((resolve, reject) => {
      Task.findById(id, (err, task) => {
        if (err) {
          ret.message = "Error";
          ret.data = "Invalid Task Id";
          res.status(404).json(ret);
          return router;
        } else {
          resolve(task);
        }
      });
    });
    promise.then((task) => {
      ret.data = task;
      ret.message = "OK";
      res.status(200).json(ret);
    })
      .catch(err => {
        console.log(err);
        ret.message = "Error";
        ret.data = "Server Error";
        res.status(500).json(ret);
        return router;
      })
  });

  // DELETE: Delete specified task or 404 error
  taskRoute.delete((req, res) => {
    var taskId = req.params.id;
    var promiseTask = new Promise((resolve, reject) => {
      Task.findByIdAndRemove(taskId, (err, task) => {
        if (err) {
          reject(err);
        } else {
          resolve(task);
        }
      });
    });
    promiseTask.then(task => {
      if (!task) {
        ret.message = "Error";
        ret.data = "Task does not exist or has been deleted";
        res.status(404).json(ret);
      } else {
        var promiseUser = new Promise((resolve, reject) => {
          User.findById(task.assignedUser, (err, user) => {
            if (err) {
              reject(err);
            } else {
              resolve(user);
            }
          });
        });
        promiseUser.then(user => {
          if (user) {
            var newTasks = user.pendingTasks.filter(value => {
              value !== taskId
            });
            User.findByIdAndUpdate({ "_id": task.assignedUser }, { $set: { pendingTasks: newTasks } })
              .then(() => {
                ret.message = "OK";
                ret.data = "The task is deleted";
                res.status(200).json(ret);
              })
              .catch(err => {
                console.log(err);
                ret.message = "Error";
                ret.data = "Server Error, cannot update User";
                res.status(500).json(ret);
              });
          }
        })
          .catch(err => {
            console.log(err);
            ret.message = "Error";
            ret.data = "Server Error, cannot find User";
            res.status(500).json(ret);
          });
      }
    })
      .catch(err => {
        console.log(err);
        ret.message = "Error";
        ret.data = "Task does not exist or has been deleted";
        res.status(404).json(ret);
      });
  });

  // PUT: Replace entire task with supplied task or 404 error
  taskRoute.put((req, res) => {
    var taskId = req.params.id;
    var taskInfo = {
      "name": req.query.name,
      "description": req.query.description,
      "deadline": req.query.deadline,
      "completed": req.query.completed,
      "assignedUser": req.query.assignedUser,
      "assignedUserName": req.query.assignedUserName,
      "dateCreated": req.query.dateCreated,
    };
    //! Tasks cannot be created (or updated) without a name or a deadline
    const invalidTaskInfo = (attribute) => {
      return typeof (attribute) === "undefined";
    }
    if (invalidTaskInfo(taskInfo.name)) {
      ret.message = "Error";
      ret.data = "Tasks cannot be created (or updated) without a name";
      res.status(404).json(ret);
      return router;
    }
    if (invalidTaskInfo(taskInfo.description)) taskInfo.description = '';
    if (invalidTaskInfo(taskInfo.deadline)) {
      ret.message = "Error";
      ret.data = "Tasks cannot be created (or updated) without a deadline";
      res.status(404).json(ret);
      return router;
    }
    if (invalidTaskInfo(taskInfo.completed)) taskInfo.completed = false;
    if (invalidTaskInfo(taskInfo.assignedUser)) taskInfo.assignedUser = '';
    if (invalidTaskInfo(taskInfo.assignedUserName)) taskInfo.assignedUserName = 'unassigned';
    if (invalidTaskInfo(taskInfo.dateCreated)) taskInfo.dateCreated = new Date();
    //* update user of old task
    Task.findById(id)
      .then(task => {
        if (!task) {
          res.status(404).send({ "message": "Error", "data": "User not found" });
          return router;
        } else {
          var previousUserID = task.assignedUser;
          if (previousUserID) {
            var previousAssignedUser = User.findByIdAndUpdate(
              previousUserID.toString(), { $pull: { "pendingTasks": task._id } }, (err, user) => {
                if (err) {
                  console.log(err);
                } else {
                  console.log("previous user task handled");
                  return user;
                }
              })
              .catch(err => {
                console.log(err);
              });
          } else {
            var previousAssignedUser = null;
          }
          //* update new assigned user
          if (invalidTaskInfo(req.query.assignedUser)) {
            console.log("invalid new assignedUser");
          } else {
            var newAssignedUser = User.findByIdAndUpdate(taskInfo.assignedUser.toString(),
              { $addToSet: { "pendingTasks": task._id } }, (err, user) => {
                if (err) {
                  console.log(err);
                } else {
                  console.log("new user task handled");
                  return user;
                }
              });
          }
          //* update task 
          if (!newAssignedUser) {
            console.log("new assigned user does not exist");
            taskInfo.assignedUser = "";
            taskInfo.assignedUserName = "unassigned";
          } else {
            taskInfo.assignedUserName = "unassigned";
          }
          Task.findByIdAndUpdate(taskId, taskInfo, (err, user) => {
            if (err) {
              console.log(err);
              ret.message = "Error";
              ret.data = "Cannot update, Server Error";
              res.status(500).json(ret);
              return router;
            } else {
              ret.message = "OK";
              ret.data = "Task successfully updated!";
              res.status(200).json(ret);
            }
          });
        }
      })
      .catch(err => {
        console.log(err);
        ret.message = "Error";
        ret.data = "Task does not exist";
        res.status(404, ret);
        return router;
      });
  });
  return router;
}