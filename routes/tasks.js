let User = require('../models/user');
let Task = require('../models/task');

module.exports = (router) => {
  let taskRoute = router.route('/tasks');
  let ret = {
    "message": "OK",
    "data": {}
  }

  // GET: Respond with a List of tasks
  taskRoute.get((req, res) => {
    var filter = {};
    if ("where" in req.query) {
      var filter = JSON.parse(req.query.where);
    }
    var sort = {};
    if ("sort" in req.query) {
      var sort = JSON.parse(req.query.sort);
    }
    var select = {};
    if ("select" in req.query) {
      var select = JSON.parse(req.query.select);
    }
    var skip = 0;
    if ("skip" in req.query) {
      var skip = JSON.parse(req.query.skip);
    }
    var limit = 0;
    if ("limit" in req.query) {
      var limit = JSON.parse(req.query.limit);
    }
    var count = false;
    if ("false" in req.query) {
      var count = JSON.parse(req.query.count);
    }
    // after filtered
    var promise = new Promise((resolve, reject) => {
      Task.find(filter, (err, tasks) => {
        if (err) {
          reject(err);
        } else {
          resolve(tasks);
        }
      })
        .sort(sort).select(select).skip(skip).limit(limit);
    });
    promise.then((tasks) => {
      ret.data = tasks;
      //! if set to true, return the count of documents that match the query (instead of the documents themselves)
      if (count) {
        ret.data = tasks.length;
      }
      ret.message = "OK";
      res.json(200, ret);
      console.log(req);
      return router;
    })
      .catch(err => {
        console.log(err);
        ret.message = "Error";
        ret.data = "Server Error";
        res.json(500, ret);
        return router;
      });
  });


  // POST: Create a new task. Respond with details of new task
  taskRoute.post((req, res) => {
    var taskInfo = {
      "name": req.param("name"),
      "description": req.param("description"),
      "deadline": req.param("deadline"),
      "completed": req.param("completed"),
      "assignedUser": req.param("assignedUser"),
      "assignedUserName": req.param("assignedUserName")
    }
    //! Tasks cannot be created (or updated) without a name or a deadline
    const invalidTaskInfo = (attribute) => {
      return typeof (attribute) === "undefined";
    }
    if (invalidTaskInfo(taskInfo.name)) {
      ret.message = "Error";
      ret.data = "Tasks cannot be created (or updated) without a name";
      res.status(404).json(ret);
      return router;
    }
    if (invalidTaskInfo(taskInfo.deadline)) {
      ret.message = "Error";
      ret.data = "Tasks cannot be created (or updated) without a deadline";
      res.status(404).json(ret);
      return router;
    }
    User.findById(taskInfo.assignedUser)
      .then(user => {
        if (user) {
          taskInfo.assignedUser = "";
          taskInfo.assignedUserName = "unassigned";
        } else {
          taskInfo.assignedUserName = user.name;
        }
      })
      .catch(err => {
        console.log(err);
      });
    //! create new task
    var newTask = new Task(taskInfo);
    newTask.save()
      .then(newTask => {
        if (!newTask.completed && newTask.assignedUser !== "") {
          User.findByIdAndUpdate(taskInfo.assignedUser, { $push: { "pendingTasks": newTask._id } })
            .catch(err => {
              console.log(err);
            })
        }
        return newTask;
      })
      .then(newTask => {
        res.status(201).send({ "message": "OK", "data": newTask });
      });
  });

  return router;
}