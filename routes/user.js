let User = require('../models/user');
let Task = require('../models/task');

module.exports = (router) => {
  let userRoute = router.route('/users/:id');
  let ret = {
    "message": "OK",
    "data": {}
  }

  // GET: Respond with details of specified user or 404 error
  // example: id: 6365a0707eaf176d88f5896a
  userRoute.get((req, res) => {
    var id = req.params.id;
    var promise = new Promise((resolve, reject) => {
      User.findById(id, (err, user) => {
        if (err) {
          ret.message = "Error";
          ret.data = "Invalid User Id";
          res.status(404).json(ret);
          return router;
        } else {
          resolve(user);
        }
      });
    });
    promise.then((user) => {
      ret.data = user;
      ret.message = "OK";
      res.status(200).json(ret);
    })
      .catch(err => {
        console.log(err);
        ret.message = "Error";
        ret.data = "Server Error";
        res.status(500).json(ret);
        return router;
      })
  });

  // DELETE: Delete specified user or 404 error
  userRoute.delete((req, res) => {
    var userId = req.params.id;
    Task.updateMany({ "assignedUser": userId }, { "assignedUser": "", "assignedUserName": "unassigned" })
      .catch(err => {
        console.log(err);
        ret.message = "Error";
        ret.data = "Server Error";
        res.status(500).json(ret);
      });
    var promise = new Promise((resolve, reject) => {
      User.findByIdAndRemove(userId, (err, user) => {
        if (err) {
          reject(err);
        } else {
          resolve(user);
        }
      });
    });
    promise.then(user => {
      if (user) {
        ret.message = "OK";
        ret.data = "User is deleted by Id";
        res.status(200).json(ret);
      } else {
        // console.log("Not deleted");
        ret.message = "Error";
        ret.data = "User does not exist";
        res.status(404).json(ret);
      }
    })
      .catch(err => {
        console.log(err);
        ret.message = "Error";
        ret.data = "User does not exist";
        res.status(404).json(ret);
      })
  });

  // PUT: Replace entire user with supplied user or 404 error
  userRoute.put((req, res) => {
    var userId = req.params.id;
    var userInfo = {
      "name": req.query.name,
      "email": req.query.email,
      "pendingTasks": req.query.pendingTasks,
      "dateCreated": req.params.dateCreated,
    };
    //! Users cannot be created (or updated) without a name or email
    const invalidUserInfo = (attribute) => {
      return typeof (attribute) === "undefined";
    }
    if (invalidUserInfo(userInfo.name)) {
      ret.message = "Error";
      ret.data = "Users cannot be created (or updated) without a name";
      res.status(404).json(ret);
      return router;
    }
    if (invalidUserInfo(userInfo.email)) {
      ret.message = "Error";
      ret.data = "Users cannot be created (or updated) without a email";
      res.status(404).json(ret);
      return router;
    }
    if (invalidUserInfo(userInfo.pendingTasks)) {
      userInfo.pendingTasks = [];
    } else {
      userInfo.pendingTasks = JSON.parse(userInfo.pendingTasks);
    }
    if (invalidUserInfo(userInfo.dateCreated)) {
      userInfo.dateCreated = new Date();
    }
    //! 404 Error when userId is invalid
    User.findById(userId)
      .then(user => {
        if (!user) {
          res.status(404).send({ "message": "Error", "data": "userId is invalid" });
          return router;
        }
      })
      .catch(err => {
        console.log(err);
        ret.message = "Error";
        ret.data = "userId is invalid";
        res.status(404).json(ret);
        return router;
      });
    //! Multiple users with the same email cannot exist
    User.findOne({ email: params["email"] })
      .then(user => {
        if (userId != user._id.toString()) {
          console.log(user._id);
          res.status(404).send({ "message": "Error", "data": "Email is invalid" });
          return router;
        } else {
          //! replace previous user, delete his task
          var previousTasks = user.pendingTasks;
          Promise.all(previousTasks.map(taskId => {
            Task.findByIdAndUpdate(taskId, { "assignedUser": '', "assignedUserName": "unassigned" })
              .catch(err => {
                console.log(err);
              });
          }))
            .then(() => {
              var newTasks = params.pendingTasks;
              Promise.all(newTasks.map(taskId => {
                var newTask = Task.findById(taskId)
                  .catch(err => {
                    console.log(err);
                  });
                return newTask;
              }))
                .then(tasks => {
                  var taskList = [];
                  tasks.forEach(task => {
                    if (task && !task.completed) {
                      taskList.push(task._id);
                      User.findByIdAndUpdate(task.assignedUser, { $pull: { "pendingTasks": task._id } })
                        .catch(err => {
                          console.log(err);
                        });
                    }
                  });
                  return taskList;
                })
                .then(taskList => {
                  params.pendingTasks = taskList;
                  User.findByIdAndUpdate(userId, params, { "new": true })
                    .then(() => {
                      Task.updateMany(
                        { "_id": { $in: params.pendingTasks } },
                        { $set: { "assignedUser": userId, "assignedUserName": params.name } }
                      )
                        .catch(err => {
                          console.log(err);
                        });
                    })
                    .then(() => {
                      res.status(201).send({ "message": "OK", "data": "PUT Success" });
                    })
                    .catch(err => {
                      console.log(err);
                    });
                })
            })
        }
      })
      .catch(err => {
        console.log(err);
      })
    return router;
  });

  return router;
} 